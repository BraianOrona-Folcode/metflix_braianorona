import express, { Request, Response } from "express";
import moviesService from "./movies.service";

const router = express.Router();

router.use(express.json());

router.get("/id/:id", async (req: Request, res: Response) => {
  const id = req?.params?.id;
  try {
    let movie = await moviesService.getById(id);
    if (!movie)
      res.status(404).send(`No se encontró el película con id: ${id}`);
    else res.json(movie);
  } catch (error) {
    res.status(404).send(`No se encontró el película con id: ${id}`);
  }
});

router.get("/find", async (req: Request, res: Response) => {
  let countries =
    typeof req?.query?.countries === "string"
      ? req.query.countries.split(",")
      : [];
  let genres =
    typeof req?.query?.genres === "string" ? req.query.genres.split(",") : [];

  if (!(countries instanceof Array) || !(genres instanceof Array))
    res.status(400).send();

  try {
    let movies = await moviesService.filter(countries, genres);

    res.json(movies);
  } catch (error) {
    res.status(404).send(`No se encontrarn pelíulas`);
  }
});

router.get("/title/:title", async (req: Request, res: Response) => {
  const title = req?.params?.title;
  try {
    let movie = await moviesService.getByTitle(title);
    if (!movie) {
      res
        .status(404)
        .send(`No se encontro la pelicula con el titulo: ${title}`);
    } else {
      res.json(movie);
    }
  } catch (error) {
    res.status(404).send(`No se encontro la pelicula con el titulo: ${title}`);
  }
});

export default router;
